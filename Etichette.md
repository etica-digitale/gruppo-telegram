Le etichette aiutano a filtrare facilmente i messaggi sul canale Telegram, e sono suddivise come segue:
  
### Tipologia
Indicano che tipo di contenuto è. Non più di uno per messaggio. Esse sono:
- **#eticadigitale**: etichetta jolly per contenuti prodotti da noi
- **#notizia**: articoli che parlano di un caso recente
- **#riflessione**: argomento di discussione che può innescare un dibattito, come gli articoli/video d'opinione
- **#risorse**: materiale utile a tema digitale
  
### Categoria
Indicano l'argomento. Possono essercene più per messaggio. Esse sono:
- **#attivismo** movimenti collettivi e azioni.
- **#autodeterminazione**: capacità dell'individuo di poter conoscere o controllare il proprio rapporto con la tecnologia
- **#censura**
- **#cripto**: blockchain, criptovalute, NFT, Rete 3.0
- **#comunicazione**: forme di comunicazione (social, messaggistica ecc.), problematiche relative (come la disinformazione) ecc.
- **#ecologia**: diritto alla riparazione, impatto ecologico delle tecnologie
- **#gsrm**: ciò che riguarda la comunità LGBT+ (Gender, Sexual, and Romantic Minorities, perché il "+" non viene preso)
- **#IA**: intelligenza artificiale, inclusi (tiratamente) algoritmi di motori di ricerca o dei social media
- **#istruzione**
- **#lavoro**
- **#leak**: per attacchi informatici dove vengono rubati e pubblicati dati degli utenti
- **#legge**: materiale legislativo (RGPD, casi giuridici ecc.)
- **#politica**: impatti sociologici, economici, politici
- **#privacy**: trattamento dei dati
- **#psiche**: ciò che riguarda l'impatto psicologico sull'individuo
- **#social**: dove i social svolgono un ruolo chiave
- **#sorveglianza**: strumenti e misure atte a sorvegliare
- **#softwarelibero**
- **#traduzioni**: contenuti tradotti da noi
- **#videogiochi**
