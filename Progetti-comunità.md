# Progetti della comunità

> Hai un progetto open source tutto tuo a cui stai lavorando? Se non contiene tracker pubblicitari e eventuali tracker restanti (come troubleshooting) sono opt-in, condividilo su https://t.me/EticaDigitale per vederlo apparire in questa lista

## Lista in ordine alfabetico

### Sviluppo videogiochi
- [PBRenderer](https://gitlab.com/NeroReflex/pbrenderer): motore di rendering che usa Vulkan
- [Spark Engine](https://github.com/NeroReflex/SparkEngine): game engine 3D  
- [Vulkan Framework](https://gitlab.com/NeroReflex/vulkan-framework): framework per Vulkan

### Videogiochi
- [S4 Steve](https://gitlab.com/s4-steve/s4-steve): hack'n'slash online ispirato a S4 League  
- [Arena_lib](https://gitlab.com/zughy-friends-minetest/arena_lib): libreria per creare minigiochi arena su Minetest  
- [Quake (Minetest)](https://gitlab.com/zughy-friends-minetest/minetest-quake): mod per simulare Quake su Minetest