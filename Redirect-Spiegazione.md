> Questo file viene linkato quando il bot del gruppo Telegram converte un link

# Cos'è successo?

Tutto normale! Il nostro bot cerca di proteggere la privacy del gruppo, e lo fa in modo molto semplice: quando viene messo un link, se questo contiene un modo per
tracciare le persone (come per la pubblicità) e il bot sa riconoscerlo, il bot cancella il messaggio e ne posta uno nuovo senza traccianti.  


## Tipi di conversione

##### YouTube -> Invidious

Invidious è un sito che specchia i contenuti di YouTube, ma orientato verso la privacy: in altre parole potete comunque guardare il video, senza regalare dati a nessuno.  
Potete inoltre convertire manualmente un video da YouTube a Invidious modificando nel link `youtube.com` in `invidio.us`. Facile, no? :D

> https://youtube.com/watch?v=PuQt9N4Dsok  ==>  https://invidio.us/watch?v=PuQt9N4Dsok (cliccare per credere)

##### Twitter -> Nitter

Stessa cosa di sopra, ma cambiando `twitter.com` con `nitter.net`.

> https://twitter.com/privacyint ==> https://nitter.net/privacyint

## Avvertenze

Sottolineiamo che il bot non rende magicamente intaccabili a livello privacy: lui ci mette il suo nella chat, ma non può — né deve — controllare il vostro browser internet o il vostro sistema operativo. Tuttavia c'è una buona notizia, ovvero che la privacy online è possibile. E se volete capire come, abbiamo realizzato una [guida passo passo](https://gitlab.com/etica-digitale/privasi/-/blob/master/README.md) :)