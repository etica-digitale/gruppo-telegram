Benvenuti nel gruppo Telegram del progetto “Etica Digitale”.  
Etica Digitale è un progetto volontario volto all’informazione e alla formazione riguardo i vari aspetti del mondo tecnologico che influiscono sulle nostre vite. Questo gruppo vuole essere un punto di ritrovo e discussione per persone da diversi ambienti ed estrazioni sociali, dove si possa vivere più da vicino la natura del nostro progetto, nel quale troverete sia curiosi alle prime armi sia programmatori. Chiunque è il benvenuto, nessuno è nato imparato.  
Gli amministratori sono a disposizione di tutti, ma ricordate che sono volontari ed hanno anche loro una vita privata e dei tempi limitati.  

Come ogni gruppo, anche questo ha il proprio regolamento al quale vi chiediamo di attenervi quanto più possibile.  

1. **Rispetto e cordialità sempre**  
Gli utenti di Etica Digitale sono svariati, ciascuno coi propri ideali, opinioni e conoscenze, per cui potresti incontrare persone ad un livello più alto del tuo come ad un livello più basso. Il gruppo vuole essere INCLUSIVO e mai esclusivo, ragion per cui sii paziente e cerca nel tuo piccolo di aiutare e costruire, anziché offendere.  
Sempre in quest'ottica, gli utenti son pregati di scrivere un sunto brevissimo riguardo un articolo quando lo si condivide. Ricordate inoltre che molti siti di giornalismo sono dietro paywall per cui considerate di copiarlo su privatebin, seppur momentaneamente. Pretendere che gli utenti accettino un paywall è maleducato e problematico, sopratutto quando è un rischio per la privacy più che per il portafogli. 

2. **Niente spam e flooding**  
(spam = pubblicità; flooding = inondare la chat con tanti brevi messaggi)<br>  
Etica Digitale è un luogo di discussione, non siamo interessati a offerte di prodotti e servizi se non sono strettamente correlati all'argomento (es. ci può interessare se avete sviluppato un'applicazione di software libero, ma la pubblicità di Glasswire risulterà in un ban istantaneo).
Per evitare inoltre di inondare gli admin e gli utenti di notifiche ma anche di seppellire argomenti, cercate di scrivere messaggi quanto più lunghi possibile per quelle che sono le vostre possibilità.

3. **Tecnicismi e discussioni lunghe**  
Sebbene questo gruppo sia nato per la discussione, la quantità di utenti e la complessità di alcuni temi ci hanno portati ad aprire il [forum](https://eticadigitale.flarum.cloud). Ogni qualvolta un discorso inizia ad allungarsi o cadere in tecnicismi (spesso è inevitabile, lo capiamo) siete pregati di creare una discussione nel forum e continuarla lì, avvisando sul gruppo ed includendo il link."

4. **Niente trolling e baiting**  
(trolling =  interagire con gli altri al solo scopo di infastidire ed innervosire; baiting = dire qualcosa di provocatorio solo per far reagire le persone e far perdere loro le staffe)<br>  
Etica Digitale è un gruppo di discussione, non un bar o una board su internet. Evitate di fare i simpaticoni con provocazioni varie. Vale anche per le risposte alle persone o agli admin.  

5. **Evitare l’off-topic (andare fuori tema)**  
L'argomento principale di Etica Digitale è appunto l'etica digitale. Parliamo di informatica in generale, dell'uso e abuso di tecnologie, di software libero e di alternative, e tutti i temi collegati.
Il gruppo non è politicizzato e non lo vuole diventare, per cui evitate discorsi con sfondi politici o propagandistici che non siano strettamente correlati agli argomenti trattati (es. può interessare parlare di net neutrality o di come possano esser stati hackerati i computer per il voto durante delle elezioni, ma scrivere “Salvini razzista” o di come tutto si risolverebbe con la completa automazione risulterà in un ban istantaneo).  

6. **Evitare attività illegali**  
Parlare di reverse engineering è contemplato; condividere materiale e pratiche illegali (pirateria, doxxing, condivisione di materiale altrui senza permesso, etc) tanto nel gruppo come in privato è severamente vietato. Vi ricordiamo che queste pratiche sono a tutti gli effetti dei crimini e in quanto tali sono legalmente perseguibili.  

7. **Non importunare gli utenti in privato**  
Se qualcuno non vuole dibattere e ha chiarito esplicitamente la sua posizione, non insistete in privato. È un comportamento irrispettoso, assillante e se portato agli estremi è passabile di denuncia.  
<br>  

Nel caso di infrazione di tali regole, a seconda della gravità lo staff si prenderà il privilegio di avvisare o bannare l'utente. Per qualsiasi domanda, informazione o segnalazione, scrivete pure all’amministrazione in privato o tramite tag.  
  

[Se volete invece postare delle notizie in modo più ordinato, date una letta qui!](https://gitlab.com/etica-digitale/gruppo-telegram/-/blob/master/Tag.md)
